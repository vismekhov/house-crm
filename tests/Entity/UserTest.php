<?php

namespace App\Tests\Entity;

use App\Entity\UserGroup;
use Mockery;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use TypeError;
use DateTime;

//@codingStandardsIgnoreFile

class UserTest extends WebTestCase
{
    /**
     * @var User
     */
    private $user;

    public function setUp(): void
    {
        $this->user = new User();

        $this->user->setEmail('test@test.test');
        $this->user->setOldPassword('old.password');
        $this->user->setPlainPassword('plain.password');
        $this->user->setPassword('plain_password.hash');
        $this->user->setFirstName('John');
        $this->user->setSecondName('Smith');
        $this->user->setApartmentNumber(99);
        $this->user->setRegisterDate(new DateTime('2020-01-01 12:00:00'));
        $this->user->setUpdatedAt(new DateTime('2020-01-01 12:00:00'));
        $this->user->setLastLogin(new DateTime('2020-01-01 12:00:00'));
        $this->user->setPhone('+380440000000');
        $this->user->setIsActive(true);
        $this->user->setIsConfirmed(true);
        $this->user->setImage('photo.jpg');
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testGetEmail()
    {
        $this->assertEquals('test@test.test', $this->user->getEmail());
        $this->assertIsString($this->user->getEmail());
    }

    public function testSetEmail()
    {
        $this->user->setEmail('foo@bar.baz');

        $this->assertNotEquals('test@test.test', $this->user->getEmail());
        $this->assertEquals('foo@bar.baz', $this->user->getEmail());
        $this->assertIsString($this->user->getEmail());
    }

    public function testSetNullEmail()
    {
        $this->expectException(TypeError::class);
        $this->user->setEmail(NULL);
    }

    public function testGetRoles()
    {
        $roles = [
            'role' => 'ROLE_USER',
        ];

        $userGroup = Mockery::mock(UserGroup::class);
        $userGroup->shouldReceive('getGroupCodes')
            ->andReturn($roles);

        $this->user->setUserGroup($userGroup);

        $this->assertEquals($userGroup->getGroupCodes(), $this->user->getRoles());
        $this->assertArrayHasKey('role', $this->user->getRoles());
        $this->assertNotNull($this->user->getRoles());
        $this->assertIsArray($this->user->getRoles());
    }

    public function testGetOldPassword()
    {
        $this->assertEquals('old.password', $this->user->getOldPassword());
        $this->assertIsString($this->user->getOldPassword());
    }

    public function testGetNullOldPassword()
    {
        $this->user->setOldPassword(NULL);

        $this->assertEquals('', $this->user->getOldPassword());
        $this->assertNull($this->user->getOldPassword());
    }

    public function testSetOldPassword()
    {
        $this->user->setOldPassword('new.password');

        $this->assertNotEquals('old.password', $this->user->getOldPassword());
        $this->assertEquals('new.password', $this->user->getOldPassword());
        $this->assertIsString($this->user->getOldPassword());
    }

    public function testSetNullOldPassword()
    {
        $this->user->setOldPassword(NULL);

        $this->assertNotEquals('old.password', $this->user->getOldPassword());
        $this->assertNull($this->user->getOldPassword());
    }

    public function testGetPlainPassword()
    {
        $this->assertEquals('plain.password', $this->user->getPlainPassword());
        $this->assertIsString($this->user->getPlainPassword());
    }

    public function testGetNullPlainPassword()
    {
        $this->user->setPlainPassword(NULL);

        $this->assertEquals('', $this->user->getPlainPassword());
        $this->assertNull($this->user->getPlainPassword());
    }

    public function testSetPlainPassword()
    {
        $this->user->setPlainPassword('new.plain.password');

        $this->assertNotEquals('plain.password', $this->user->getPlainPassword());
        $this->assertEquals('new.plain.password', $this->user->getPlainPassword());
        $this->assertIsString($this->user->getPlainPassword());
    }

    public function testSetNullPlainPassword()
    {
        $this->user->setPlainPassword(NULL);

        $this->assertNotEquals('plain.password', $this->user->getPlainPassword());
        $this->assertNull($this->user->getPlainPassword());
    }

    public function testGetPassword()
    {
        $this->assertEquals('plain_password.hash', $this->user->getPassword());
        $this->assertIsString($this->user->getPassword());
    }

    public function testSetPassword()
    {
        $plainPassword = 'foo.bar.baz';

        $passwordEncoder = Mockery::mock(UserPasswordEncoder::class);
        $passwordEncoder->shouldReceive('encodePassword')
            ->andReturn($plainPassword.'.hash');

        $encodedPassword = $passwordEncoder->encodePassword($this->user, $plainPassword);
        $this->user->setPassword($encodedPassword);

        $this->assertNotEquals('plain_password.hash', $this->user->getPassword());
        $this->assertEquals('foo.bar.baz.hash', $this->user->getPassword());
        $this->assertIsString($this->user->getPassword());
    }

    public function testSetNullPassword()
    {
        $this->expectException(TypeError::class);
        $this->user->setPassword(NULL);
    }

    public function testGetFirstName()
    {
        $this->assertEquals('John', $this->user->getFirstName());
        $this->assertIsString($this->user->getFirstName());
    }

    public function testGetNullFirstName()
    {
        $this->user->setFirstName(NULL);

        $this->assertEquals('', $this->user->getFirstName());
        $this->assertNull($this->user->getFirstName());
    }

    public function testSetFirstName()
    {
        $this->user->setFirstName('James');

        $this->assertNotEquals('John', $this->user->getFirstName());
        $this->assertEquals('James', $this->user->getFirstName());
        $this->assertIsString($this->user->getFirstName());
    }

    public function testSetNullFirstName()
    {
        $this->user->setFirstName(NULL);

        $this->assertNotEquals('John', $this->user->getFirstName());
        $this->assertNull($this->user->getFirstName());
    }

    public function testGetSecondName()
    {
        $this->assertEquals('Smith', $this->user->getSecondName());
        $this->assertIsString($this->user->getSecondName());
    }

    public function testGetNullSecondName()
    {
        $this->user->setSecondName(NULL);

        $this->assertEquals('', $this->user->getSecondName());
        $this->assertNull($this->user->getSecondName());
    }

    public function testSetSecondName()
    {
        $this->user->setSecondName('McCarthy');

        $this->assertNotEquals('Smith', $this->user->getSecondName());
        $this->assertEquals('McCarthy', $this->user->getSecondName());
        $this->assertIsString($this->user->getSecondName());
    }

    public function testSetNullSecondName()
    {
        $this->user->setSecondName(NULL);

        $this->assertNotEquals('Smith', $this->user->getSecondName());
        $this->assertNull($this->user->getSecondName());
    }

    public function testSetApartmentNumber()
    {
        $this->user->setApartmentNumber(1);

        $this->assertNotEquals(99, $this->user->getApartmentNumber());
        $this->assertEquals(1, $this->user->getApartmentNumber());
        $this->assertIsString($this->user->getApartmentNumber());
    }

    public function testGetApartmentNumber()
    {
        $this->assertEquals(99, $this->user->getApartmentNumber());
        $this->assertIsString($this->user->getApartmentNumber());
    }

    public function testSetNullApartmentNumber()
    {
        $this->expectException(TypeError::class);
        $this->user->setApartmentNumber(NULL);
    }

    public function testGetLastLogin()
    {
        $this->assertEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getLastLogin());
        $this->assertIsObject($this->user->getLastLogin());
    }

    public function testGetNullLastLogin()
    {
        $this->user->setLastLogin(NULL);

        $this->assertEquals(NULL, $this->user->getLastLogin());
        $this->assertNull($this->user->getLastLogin());
    }

    public function testSetLastLogin()
    {
        $this->user->setLastLogin(new DateTime('2021-01-01 12:00:00'));

        $this->assertNotEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getLastLogin());
        $this->assertEquals(new DateTime('2021-01-01 12:00:00'), $this->user->getLastLogin());
        $this->assertIsObject($this->user->getLastLogin());
    }

    public function testSetNullLastLogin()
    {
        $this->user->setLastLogin(NULL);

        $this->assertNotEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getLastLogin());
        $this->assertNull($this->user->getLastLogin());
    }

    public function testSetPhone()
    {
        $this->user->setPhone('+380441111111');

        $this->assertNotEquals('+380440000000', $this->user->getPhone());
        $this->assertEquals('+380441111111', $this->user->getPhone());
        $this->assertIsString($this->user->getPhone());
    }

    public function testGetPhone()
    {
        $this->assertEquals('+380440000000', $this->user->getPhone());
        $this->assertIsString($this->user->getPhone());
    }

    public function testSetNullPhone()
    {
        $this->expectException(TypeError::class);
        $this->user->setPhone(NULL);
    }

    public function testSetIsActive()
    {
        $this->user->setIsActive(false);

        $this->assertNotEquals(true, $this->user->getIsActive());
        $this->assertEquals(false, $this->user->getIsActive());
        $this->assertIsBool($this->user->getIsActive());
    }

    public function testGetIsActive()
    {
        $this->assertEquals(true, $this->user->getIsActive());
        $this->assertIsBool($this->user->getIsActive());
    }

    public function testSetNullIsActive()
    {
        $this->expectException(TypeError::class);
        $this->user->setIsActive(NULL);
    }

    public function testGetUserGroup()
    {
        $userGroup = Mockery::mock(UserGroup::class);
        $this->user->setUserGroup($userGroup);

        $this->assertEquals($userGroup, $this->user->getUserGroup());
        $this->assertNotNull($this->user->getUserGroup());
        $this->assertIsObject($this->user->getUserGroup());
    }

    public function testSetUserGroup()
    {
        $userGroup = Mockery::mock(UserGroup::class);
        $this->user->setUserGroup($userGroup);

        $this->assertEquals($userGroup, $this->user->getUserGroup());
        $this->assertNotNull($this->user->getUserGroup());
        $this->assertIsObject($this->user->getUserGroup());
    }

    public function testGetRegisterDate()
    {
        $this->assertEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getRegisterDate());
        $this->assertIsObject($this->user->getRegisterDate());
    }

    public function testGetNullRegisterDate()
    {
        $this->expectException(TypeError::class);
        $this->user->setRegisterDate(NULL);
    }

    public function testSetRegisterDate()
    {
        $this->user->setRegisterDate(new DateTime('2021-01-01 12:00:00'));

        $this->assertNotEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getRegisterDate());
        $this->assertEquals(new DateTime('2021-01-01 12:00:00'), $this->user->getRegisterDate());
        $this->assertIsObject($this->user->getRegisterDate());
    }

    public function testSetNullRegisterDate()
    {
        $this->expectException(TypeError::class);
        $this->user->setRegisterDate(NULL);
    }

    public function testSetIsConfirmed()
    {
        $this->user->setIsConfirmed(false);

        $this->assertNotEquals(true, $this->user->getIsConfirmed());
        $this->assertEquals(false, $this->user->getIsConfirmed());
        $this->assertIsBool($this->user->getIsConfirmed());
    }

    public function testGetIsConfirmed()
    {
        $this->assertEquals(true, $this->user->getIsConfirmed());
        $this->assertIsBool($this->user->getIsConfirmed());
    }

    public function testSetNullIsConfirmed()
    {
        $this->expectException(TypeError::class);
        $this->user->setIsConfirmed(NULL);
    }

    public function testGetUpdatedAt()
    {
        $this->assertEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getUpdatedAt());
        $this->assertIsObject($this->user->getUpdatedAt());
    }

    public function testGetNullUpdatedAt()
    {
        $this->user->setUpdatedAt(NULL);

        $this->assertEquals(NULL, $this->user->getUpdatedAt());
        $this->assertNull($this->user->getUpdatedAt());
    }

    public function testSetUpdatedAt()
    {
        $this->user->setUpdatedAt(new DateTime('2021-01-01 12:00:00'));

        $this->assertNotEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getUpdatedAt());
        $this->assertEquals(new DateTime('2021-01-01 12:00:00'), $this->user->getUpdatedAt());
        $this->assertIsObject($this->user->getUpdatedAt());
    }

    public function testSetNullUpdatedAt()
    {
        $this->user->setUpdatedAt(NULL);

        $this->assertNotEquals(new DateTime('2020-01-01 12:00:00'), $this->user->getUpdatedAt());
        $this->assertNull($this->user->getUpdatedAt());
    }

    public function testGetImage()
    {
        $this->assertEquals('photo.jpg', $this->user->getImage());
        $this->assertIsString($this->user->getImage());
    }

    public function testGetNullImage()
    {
        $this->user->setImage(NULL);

        $this->assertEquals('', $this->user->getImage());
        $this->assertNull($this->user->getImage());
    }

    public function testSetImage()
    {
        $this->user->setImage('photo2.jpg');

        $this->assertNotEquals('photo.jpg', $this->user->getImage());
        $this->assertEquals('photo2.jpg', $this->user->getImage());
        $this->assertIsString($this->user->getImage());
    }

    public function testSetNullImage()
    {
        $this->user->setImage(NULL);

        $this->assertNotEquals('photo.jpg', $this->user->getImage());
        $this->assertNull($this->user->getImage());
    }
}
