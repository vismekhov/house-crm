<?php

namespace App\Tests\Entity;

use App\Entity\UserGroup;
use Mockery;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TypeError;

//@codingStandardsIgnoreFile

class UserGroupTest extends WebTestCase
{
    /**
     * @var UserGroup
     */
    private $userGroup;

    public function setUp(): void
    {
        $this->userGroup = new UserGroup();

        $this->userGroup->setName('testGroup');
        $this->userGroup->setGroupCodes(['role' => 'ROLE_USER']);
        $this->userGroup->setDescription('testDescription');
        $this->userGroup->setIsActive(true);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    public function testGetName()
    {
        $this->assertEquals('testGroup', $this->userGroup->getName());
        $this->assertIsString($this->userGroup->getName());
    }

    public function testSetName()
    {
        $this->userGroup->setName('fooBarBaz');

        $this->assertNotEquals('testGroup', $this->userGroup->getName());
        $this->assertEquals('fooBarBaz', $this->userGroup->getName());
        $this->assertIsString($this->userGroup->getName());
    }

    public function testSetNullName()
    {
        $this->expectException(TypeError::class);
        $this->userGroup->setName(NULL);
    }

    public function testGetGroupCodes()
    {
        $this->assertEquals(['role' => 'ROLE_USER'], $this->userGroup->getGroupCodes());
        $this->assertArrayHasKey('role', $this->userGroup->getGroupCodes());
        $this->assertNotNull($this->userGroup->getGroupCodes());
        $this->assertIsArray($this->userGroup->getGroupCodes());
    }

    public function testGetNullGroupCodes()
    {
        $this->expectException(TypeError::class);
        $this->userGroup->setGroupCodes(NULL);
    }

    public function testSetGroupCodes()
    {
        $this->userGroup->setGroupCodes(['role' => 'ROLE_ADMIN']);

        $this->assertNotEquals(['role' => 'ROLE_USER'], $this->userGroup->getGroupCodes());
        $this->assertEquals(['role' => 'ROLE_ADMIN'], $this->userGroup->getGroupCodes());
        $this->assertArrayHasKey('role', $this->userGroup->getGroupCodes());
        $this->assertNotNull($this->userGroup->getGroupCodes());
        $this->assertIsArray($this->userGroup->getGroupCodes());
    }

    public function testSetNullGroupCodes()
    {
        $this->expectException(TypeError::class);
        $this->userGroup->setGroupCodes(NULL);
    }

    public function testGetDescription()
    {
        $this->assertEquals('testDescription', $this->userGroup->getDescription());
        $this->assertIsString($this->userGroup->getDescription());
    }

    public function testGetNullDescription()
    {
        $this->userGroup->setDescription(NULL);

        $this->assertEquals('', $this->userGroup->getDescription());
        $this->assertNull($this->userGroup->getDescription());
    }

    public function testSetDescription()
    {
        $this->userGroup->setDescription('fooBarBaz');

        $this->assertNotEquals('testDescription', $this->userGroup->getDescription());
        $this->assertEquals('fooBarBaz', $this->userGroup->getDescription());
        $this->assertIsString($this->userGroup->getDescription());
    }

    public function testSetNullDescription()
    {
        $this->userGroup->setDescription(NULL);

        $this->assertNotEquals('testDescription', $this->userGroup->getDescription());
        $this->assertNull($this->userGroup->getDescription());
    }

    public function testSetIsActive()
    {
        $this->userGroup->setIsActive(false);

        $this->assertNotEquals(true, $this->userGroup->getIsActive());
        $this->assertEquals(false, $this->userGroup->getIsActive());
        $this->assertIsBool($this->userGroup->getIsActive());
    }

    public function testGetIsActive()
    {
        $this->assertEquals(true, $this->userGroup->getIsActive());
        $this->assertIsBool($this->userGroup->getIsActive());
    }

    public function testSetNullIsActive()
    {
        $this->expectException(TypeError::class);
        $this->userGroup->setIsActive(NULL);
    }
}
