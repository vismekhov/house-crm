<?php

namespace App\Controller\Admin;

use App\Entity\Contacts;
use App\Form\Admin\ContactsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactsController
 *
 * @Route("/admin/contacts")
 */
class ContactsController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/", name="admin_contacts")
     */
    public function contacts(): Response
    {
        $managementContacts = $this->getDoctrine()
            ->getRepository(Contacts::class)
            ->getManagementContacts();

        $personalContacts = $this->getDoctrine()
            ->getRepository(Contacts::class)
            ->getPersonalContacts();

        return $this->render('admin/contacts/index.html.twig', [
            'managementContacts' => $managementContacts,
            'personalContacts' => $personalContacts,
        ]);
    }

    /**
     * @param Contacts $entity
     * @param Request  $request
     *
     * @return Response
     *
     * @Route("/edit/{id}", name="edit_contact")
     */
    public function editContact(Contacts $entity, Request $request): Response
    {
        $form = $this->createForm(ContactsType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            $this->addFlash('success', 'Successful contacts edit');

            return $this->redirectToRoute('admin_contacts');
        }

        return $this->render('admin/contacts/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/create", name="create_contact")
     */
    public function newContact(Request $request): Response
    {
        $newContact = new Contacts();
        $form = $this->createForm(ContactsType::class, $newContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newContact);
            $entityManager->flush();

            $this->addFlash('success', 'Successful contacts create');

            return $this->redirectToRoute('admin_contacts');
        }

        return $this->render('admin/contacts/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/delete_contact", name="delete_contact")
     */
    public function ajaxDeleteContact(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $contactId = $request->get('contactId');

        $contact = $this->getDoctrine()
            ->getRepository(Contacts::class)
            ->find($contactId);

        if (!$contact) {
            return new JsonResponse('No contact found.', 500);
        }

        $entityManager->remove($contact);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }
}
