<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Service\EmailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @Route("/admin/users")
 */
class UserController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/active", name="users_active")
     */
    public function active(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(User::class)
            ->getActiveUsers();

        return $this->render('admin/user/active.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @return Response
     *
     * @Route("/new", name="users_new")
     */
    public function new(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(User::class)
            ->getNewUsers();

        return $this->render('admin/user/new.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @return Response
     *
     * @Route("/blocked", name="users_blocked")
     */
    public function blocked(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(User::class)
            ->getBlockedUsers();

        return $this->render('admin/user/blocked.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @param Request      $request
     * @param EmailService $emailService
     *
     * @return JsonResponse
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @Route("/ajax/approve-user", name="ajax_approve_user")
     */
    public function approveUser(Request $request, EmailService $emailService): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $request->get('userId');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);

        if (!$user) {
            return new JsonResponse('No user found.', 500);
        }

        $user->setIsActive(true);
        $user->setIsConfirmed(true);
        $entityManager->persist($user);
        $entityManager->flush();

        $emailService->send(EmailService::APPROVE_REGISTRATION, $user->getEmail());

        return new JsonResponse('success', 200);
    }

    /**
     * @param Request      $request
     * @param EmailService $emailService
     *
     * @return JsonResponse
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @Route("/ajax/block-user", name="ajax_block_user")
     */
    public function blockUser(Request $request, EmailService $emailService): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $request->get('userId');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);

        if (!$user) {
            return new JsonResponse('No user found.', 500);
        }

        $user->setIsActive(false);
        $user->setIsConfirmed(true);
        $entityManager->persist($user);
        $entityManager->flush();

        $emailService->send(EmailService::BLOCK_USER, $user->getEmail());

        return new JsonResponse('success', 200);
    }

    /**
     * @param Request      $request
     * @param EmailService $emailService
     *
     * @return JsonResponse
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @Route("/ajax/unblock-user", name="ajax_unblock_user")
     */
    public function unblockUser(Request $request, EmailService $emailService): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $request->get('userId');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);

        if (!$user) {
            return new JsonResponse('No user found.', 500);
        }

        $user->setIsActive(true);
        $user->setIsConfirmed(true);
        $entityManager->persist($user);
        $entityManager->flush();

        $emailService->send(EmailService::UNBLOCK_USER, $user->getEmail());

        return new JsonResponse('success', 200);
    }

    /**
     * @param Request      $request
     * @param EmailService $emailService
     *
     * @return JsonResponse
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @Route("/ajax/decline-user", name="ajax_decline_user")
     */
    public function declineUser(Request $request, EmailService $emailService): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $request->get('userId');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);

        if (!$user) {
            return new JsonResponse('No user found.', 500);
        }

        $emailService->send(EmailService::DECLINE_REGISTRATION, $user->getEmail());

        $entityManager->remove($user);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }
}
