<?php

namespace App\Controller\Admin;

use App\Entity\Feedback;
use App\Entity\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FeedbackController
 *
 * @Route("/admin/messages")
 */
class FeedbackController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/new", name="new_messages")
     */
    public function newMessages(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $messages = $entityManager->getRepository(Feedback::class)
            ->getNewMessages();

        return $this->render('admin/feedback/new.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @return Response
     *
     * @Route("/read", name="read_messages")
     */
    public function readMessages(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $messages = $entityManager->getRepository(Feedback::class)
            ->getReadMessages();

        return $this->render('admin/feedback/read.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/make-message-read", name="make_message_read")
     */
    public function makeMessageRead(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $messageId = $request->get('messageId');

        $feedback = $this->getDoctrine()
            ->getRepository(Feedback::class)
            ->find($messageId);

        if (!$feedback) {
            return new JsonResponse('No feedback found.', 500);
        }

        $feedbackType = $this->getDoctrine()
            ->getRepository(FeedbackType::class)
            ->findOneBy([
                'code' => FeedbackType::READ_FEEDBACK,
            ]);

        $feedback->setType($feedbackType);
        $entityManager->persist($feedback);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }
}
