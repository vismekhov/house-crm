<?php

namespace App\Controller\Admin;

use App\Entity\Advertisement;
use App\Entity\AdvertisementAnswer;
use App\Entity\AdvertisementQuestion;
use App\Form\Admin\AdvertisementType;
use App\Form\Admin\AdvertisementAnswerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/advertisements")
 */
class AdvertisementController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/", name="admin_advertisements")
     */
    public function index(): Response
    {
        $advertisements = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->getAdvertisementsByFiveBatches(0);

        return $this->render('admin/advertisement/index.html.twig', [
            'advertisements' => $advertisements,
        ]);
    }

    /**
     * @return Response
     *
     * @Route("/questions", name="admin_advertisement_questions_list")
     */
    public function questionsList(): Response
    {
        $advertisements = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->getAdvertisementsByFiveBatches(0);

        return $this->render('admin/advertisement/questions.html.twig', [
            'advertisements' => $advertisements,
        ]);
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/questions/{id}", name="admin_advertisement_questions")
     */
    public function advertisementQuestions(int $id, Request $request): Response
    {
        $answers = $this->getDoctrine()
            ->getRepository(AdvertisementAnswer::class)
            ->getAnswersByAdvertisement($id);

        $newAdvertisementAnswer = new AdvertisementAnswer();
        $form = $this->createForm(AdvertisementAnswerType::class, $newAdvertisementAnswer, [
            'advertisementId' => $id,
            'questionsType' => AdvertisementQuestion::TYPE_NOT_ANSWERED,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newAdvertisementAnswer);
            $entityManager->flush();

            $this->addFlash('success', 'Successful new question answer');

            return $this->redirectToRoute('admin_advertisement_questions', [
                'id' => $id,
            ]);
        }

        return $this->render('admin/advertisement/edit_questions.html.twig', [
            'answers' => $answers,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param AdvertisementAnswer $advertisementAnswer
     * @param Request             $request
     *
     * @return Response
     *
     * @Route("/answer/{id}", name="admin_advertisement_answer")
     */
    public function advertisementAnswers(AdvertisementAnswer $advertisementAnswer, Request $request): Response
    {
        $form = $this->createForm(AdvertisementAnswerType::class, $advertisementAnswer, [
            'advertisementId' => $advertisementAnswer->getAdvertisement()->getId(),
            'questionsType' => AdvertisementQuestion::TYPE_ANSWERED,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertisementAnswer);
            $entityManager->flush();

            $this->addFlash('success', 'Successful question answer edit');

            return $this->redirectToRoute('admin_advertisement_questions', [
                'id' => $advertisementAnswer->getAdvertisement()->getId(),
            ]);
        }

        return $this->render('admin/advertisement/edit_answer.html.twig', [
            'advertisementAnswer' => $advertisementAnswer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Advertisement $advertisement
     * @param Request       $request
     *
     * @return Response
     *
     * @Route("/edit/{id}", name="admin_edit_advertisement")
     */
    public function edit(Advertisement $advertisement, Request $request): Response
    {
        $form = $this->createForm(AdvertisementType::class, $advertisement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertisement);
            $entityManager->flush();

            $this->addFlash('success', 'Successful advertisement edit');

            return $this->redirectToRoute('admin_advertisements');
        }

        return $this->render('admin/advertisement/edit.html.twig', [
            'advertisement' => $advertisement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/create", name="admin_create_advertisement")
     */
    public function create(Request $request): Response
    {
        $advertisement = new Advertisement();
        $form = $this->createForm(AdvertisementType::class, $advertisement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertisement);
            $entityManager->flush();

            $this->addFlash('success', 'Successful advertisement create');

            return $this->redirectToRoute('admin_advertisements');
        }

        return $this->render('admin/advertisement/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/load-more-advertisements", name="admin_load_more_advertisements")
     */
    public function ajaxLoadMoreAdvertisements(Request $request): JsonResponse
    {
        $from = $request->get('from');

        $entities = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->getAdvertisementsByFiveBatches($from);

        $result = [];
        foreach ($entities as $entity) {
            $result[] = [
                'id' => $entity->getId(),
                'title' => $entity->getTitle(),
                'recap' => $entity->getRecap(),
                'createdDate' => $entity->getCreatedDate()->format('Y-m-d'),
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/delete_advertisement", name="delete_advertisement")
     */
    public function ajaxDeleteAdvertisement(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $advertisementId = $request->get('advertisementId');

        $advertisement = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->find($advertisementId);

        if (!$advertisement) {
            return new JsonResponse('No advertisement found.', 500);
        }

        $entityManager->remove($advertisement);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/delete_advertisement_question", name="delete_advertisement_question")
     */
    public function ajaxDeleteAdvertisementQuestion(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $questionId = $request->get('questionId');

        $question = $this->getDoctrine()
            ->getRepository(AdvertisementQuestion::class)
            ->find($questionId);

        if (!$question) {
            return new JsonResponse('No question found.', 500);
        }

        $entityManager->remove($question);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/delete_advertisement_answer", name="delete_advertisement_answer")
     */
    public function ajaxDeleteAdvertisementAnswer(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $answerId = $request->get('answerId');

        $answer = $this->getDoctrine()
            ->getRepository(AdvertisementAnswer::class)
            ->find($answerId);

        if (!$answer) {
            return new JsonResponse('No answer found.', 500);
        }

        $entityManager->remove($answer);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }
}
