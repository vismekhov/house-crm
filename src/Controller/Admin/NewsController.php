<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Form\Admin\NewsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/news")
 */
class NewsController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/", name="admin_news")
     */
    public function index(): Response
    {
        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->getNewsByFiveBatches(0);

        return $this->render('admin/news/index.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @param News    $news
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/edit/{id}", name="admin_edit_news")
     */
    public function edit(News $news, Request $request): Response
    {
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            $this->addFlash('success', 'Successful news edit');

            return $this->redirectToRoute('admin_news');
        }

        return $this->render('admin/news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/create", name="admin_create_news")
     */
    public function create(Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            $this->addFlash('success', 'Successful news create');

            return $this->redirectToRoute('admin_news');
        }

        return $this->render('admin/news/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/load-more-news", name="admin_load_more_news")
     */
    public function ajaxLoadMoreNews(Request $request): JsonResponse
    {
        $from = $request->get('from');

        $entities = $this->getDoctrine()
            ->getRepository(News::class)
            ->getNewsByFiveBatches($from);

        $result = [];
        foreach ($entities as $entity) {
            $result[] = [
                'id' => $entity->getId(),
                'title' => $entity->getTitle(),
                'recap' => $entity->getRecap(),
                'createdDate' => $entity->getCreatedDate()->format('Y-m-d'),
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/delete_news", name="delete_news")
     */
    public function ajaxDeleteNews(Request $request): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newsId = $request->get('newsId');

        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->find($newsId);

        if (!$news) {
            return new JsonResponse('No news found.', 500);
        }

        $entityManager->remove($news);
        $entityManager->flush();

        return new JsonResponse('success', 200);
    }
}
