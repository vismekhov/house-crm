<?php

namespace App\Controller\Client\User;

use App\Entity\User;
use App\Form\Client\RegistrationFormType;
use App\Service\EmailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RegistrationController
 *
 * @Route("/register")
 */
class RegistrationController extends AbstractController
{
    /**
     * @param Request      $request
     * @param EmailService $emailService
     *
     * @return Response
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @Route("/", name="app_register")
     */
    public function register(Request $request, EmailService $emailService): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Successful registration request');
            $emailService->send(EmailService::INITIAL_REGISTRATION, $user->getEmail());

            return $this->redirectToRoute('index');
        }

        return $this->render('client/user/registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/check-registartion-data-dublicates", name="check_registration_data_duplicates")
     */
    public function checkRegistrationDataDuplicates(Request $request): JsonResponse
    {
        $email = $request->get('email');

        $isEmailExists = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy([
                'email' => $email,
            ]);

        if ($isEmailExists) {
            return new JsonResponse('email exists', 500);
        }

        return new JsonResponse('error', 200);
    }
}
