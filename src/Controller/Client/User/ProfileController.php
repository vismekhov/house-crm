<?php

namespace App\Controller\Client\User;

use App\Entity\EntityInterface\UserEntityInterface;
use App\Form\Client\ChangePasswordType;
use App\Form\Client\ProfileType;
use App\Service\UserPasswordService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;
use Vich\UploaderBundle\Handler\UploadHandler;

/**
 * Class ProfileController
 *
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="profile")
     */
    public function index(Request $request): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('profile');
        }

        return $this->render('client/user/profile/index.html.twig', [
            'profileForm' => $form->createView(),
        ]);
    }

    /**
     * @param Request             $request
     * @param UserPasswordService $userPasswordService
     *
     * @return Response
     *
     * @throws Exception
     *
     * @Route("/password", name="profile_passport_change")
     */
    public function passwordChange(Request $request, UserPasswordService $userPasswordService): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userPasswordService->changeUserPassword($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_logout');
        }

        return $this->render('client/user/profile/password_change.html.twig', [
            'changePasswordForm' => $form->createView(),
        ]);
    }

    /**
     * @param Request       $request
     * @param UploadHandler $uploadHandler
     *
     * @return JsonResponse
     *
     * @Route("/ajax/remove-photo", name="remove_photo")
     */
    public function removeUserPhoto(Request $request, UploadHandler $uploadHandler): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof UserEntityInterface) {
            return new JsonResponse('error', 200);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $user->setImage(null);
        $user->setImageFile(null);
        $uploadHandler->remove($user, 'imageFile');

        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse('success', 500);
    }
}
