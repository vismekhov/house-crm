<?php

namespace App\Controller\Client\Contacts;

use App\Entity\Contacts;
use App\Entity\Feedback;
use App\Form\Client\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contacts")
 */
class ContactsController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="contacts")
     */
    public function index(Request $request): Response
    {
        $feedback = new Feedback();

        $managementContacts = $this->getDoctrine()
            ->getRepository(Contacts::class)
            ->getManagementContacts();

        $personalContacts = $this->getDoctrine()
            ->getRepository(Contacts::class)
            ->getPersonalContacts();

        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($feedback);
            $entityManager->flush();

            $this->addFlash('success', 'Successful feedback');

            return $this->redirectToRoute('contacts');
        }

        return $this->render('client/contacts/index.html.twig', [
            'managementContacts' => $managementContacts,
            'personalContacts' => $personalContacts,
            'feedbackForm' => $form->createView(),
        ]);
    }
}
