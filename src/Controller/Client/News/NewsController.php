<?php

namespace App\Controller\Client\News;

use App\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news")
 */
class NewsController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/", name="news")
     */
    public function index(): Response
    {
        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->getNewsByFiveBatches(0);

        return $this->render('client/news/index.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @param int $id
     *
     * @return Response
     *
     * @Route("/{id}", name="read_news")
     */
    public function read(int $id): Response
    {
        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->find($id);

        return $this->render('client/news/detailed.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/load-more-news", name="load_more_news")
     */
    public function ajaxLoadMoreNews(Request $request): JsonResponse
    {
        $from = $request->get('from');

        $entities = $this->getDoctrine()
            ->getRepository(News::class)
            ->getNewsByFiveBatches($from);

        $result = [];
        foreach ($entities as $entity) {
            $result[] = [
                'id' => $entity->getId(),
                'title' => $entity->getTitle(),
                'recap' => $entity->getRecap(),
                'createdDate' => $entity->getCreatedDate()->format('Y-m-d'),
            ];
        }

        return new JsonResponse($result);
    }
}
