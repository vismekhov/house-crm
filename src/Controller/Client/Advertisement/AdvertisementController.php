<?php

namespace App\Controller\Client\Advertisement;

use App\Entity\Advertisement;
use App\Entity\AdvertisementAnswer;
use App\Entity\AdvertisementQuestion;
use App\Form\Client\AdvertisementQuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/advertisements")
 */
class AdvertisementController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("/", name="advertisements")
     */
    public function index(): Response
    {
        $advertisements = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->getAdvertisementsByFiveBatches(0);

        return $this->render('client/advertisement/index.html.twig', [
            'advertisements' => $advertisements,
        ]);
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/{id}", name="read_advertisements")
     */
    public function read(int $id, Request $request): Response
    {
        $advertisement = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->find($id);

        $questions = $this->getDoctrine()
            ->getRepository(AdvertisementAnswer::class)
            ->findByAdvertisement($id);

        $newAdvertisementQuestion = new AdvertisementQuestion();
        $form = $this->createForm(AdvertisementQuestionType::class, $newAdvertisementQuestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newAdvertisementQuestion);
            $entityManager->flush();

            $this->addFlash('success', 'Successful new advertisement question');

            return $this->redirectToRoute('read_advertisements', [
                'id' => $id,
            ]);
        }

        return $this->render('client/advertisement/detailed.html.twig', [
            'advertisement' => $advertisement,
            'questions' => $questions,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/ajax/load-more-advertisements", name="load_more_advertisements")
     */
    public function ajaxLoadMoreAdvertisements(Request $request): JsonResponse
    {
        $from = $request->get('from');

        $entities = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->getAdvertisementsByFiveBatches($from);

        $result = [];
        foreach ($entities as $entity) {
            $result[] = [
                'id' => $entity->getId(),
                'title' => $entity->getTitle(),
                'recap' => $entity->getRecap(),
                'createdDateDM' => $entity->getCreatedDate()->format('d M'),
                'createdDateY' => $entity->getCreatedDate()->format('Y'),
            ];
        }

        return new JsonResponse($result);
    }
}
