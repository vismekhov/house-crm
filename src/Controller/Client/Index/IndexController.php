<?php

namespace App\Controller\Client\Index;

use App\Entity\Advertisement;
use App\Entity\Contacts;
use App\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $advertisements = $this->getDoctrine()
            ->getRepository(Advertisement::class)
            ->getAdvertisementsOnIndex();

        $news = $this->getDoctrine()
            ->getRepository(News::class)
            ->getNewsOnIndex();

        $personalContacts = $this->getDoctrine()
            ->getRepository(Contacts::class)
            ->getPersonalContacts();

        return $this->render('client/index/index.html.twig', [
            'advertisements' => $advertisements,
            'news' => $news,
            'personalContacts' => $personalContacts,
        ]);
    }
}
