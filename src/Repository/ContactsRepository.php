<?php

namespace App\Repository;

use App\Entity\Contacts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Contacts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contacts|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contacts[]    findAll()
 * @method Contacts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactsRepository extends ServiceEntityRepository
{
    /**
     * ContactsRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contacts::class);
    }

    /**
     * @return mixed
     */
    public function getManagementContacts()
    {
        $qb = $this->createQueryBuilder('contacts')
            ->where('contacts.isManagement = true')
            ->orderBy('contacts.id', 'ASC')
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getPersonalContacts()
    {
        $qb = $this->createQueryBuilder('contacts')
            ->where('contacts.isManagement = false')
            ->orderBy('contacts.title', 'ASC')
        ;

        return $qb->getQuery()->execute();
    }
}
