<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @param int $from
     *
     * @return mixed
     */
    public function getNewsByFiveBatches(int $from)
    {
        $qb = $this->createQueryBuilder('news')
            ->setMaxResults(5)
            ->setFirstResult($from)
            ->orderBy('news.createdDate', 'DESC')
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getNewsOnIndex()
    {
        $qb = $this->createQueryBuilder('news')
            ->setMaxResults(5)
            ->orderBy('news.createdDate', 'DESC')
        ;

        return $qb->getQuery()->execute();
    }
}
