<?php

namespace App\Repository;

use App\Entity\AdvertisementAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AdvertisementAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdvertisementAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdvertisementAnswer[]    findAll()
 * @method AdvertisementAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertisementAnswerRepository extends ServiceEntityRepository
{
    /**
     * AdvertisementAnswerRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdvertisementAnswer::class);
    }

    /**
     * @param int $advertisementId
     *
     * @return mixed
     */
    public function getAnswersByAdvertisement(int $advertisementId)
    {
        $qb = $this->createQueryBuilder('answers')
            ->where('answers.advertisement = :advertisementId')
            ->setParameter('advertisementId', $advertisementId)
        ;

        return $qb->getQuery()->execute();
    }
}
