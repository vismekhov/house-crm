<?php

namespace App\Repository;

use App\Entity\Advertisement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Advertisement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advertisement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advertisement[]    findAll()
 * @method Advertisement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertisementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advertisement::class);
    }

    /**
     * @param int $from
     *
     * @return mixed
     */
    public function getAdvertisementsByFiveBatches(int $from)
    {
        $qb = $this->createQueryBuilder('ads')
            ->setMaxResults(5)
            ->setFirstResult($from)
            ->orderBy('ads.createdDate', 'DESC')
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getAdvertisementsOnIndex()
    {
        $qb = $this->createQueryBuilder('ads')
            ->setMaxResults(3)
            ->orderBy('ads.createdDate', 'DESC')
        ;

        return $qb->getQuery()->execute();
    }
}
