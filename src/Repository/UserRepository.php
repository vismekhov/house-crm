<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class UserRepository
 */
class UserRepository extends ServiceEntityRepository
{
    public const ROOT_USER = 'root@root.root';

    /**
     * UserRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return mixed
     */
    public function getActiveUsers()
    {
        $qb = $this->createQueryBuilder('user')
            ->where('user.isActive = true')
            ->andWhere('user.isConfirmed = true')
            ->andWhere('user.email != :rootUser')
            ->orderBy('user.apartmentNumber', 'ASC')
            ->setParameter('rootUser', self::ROOT_USER)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getNewUsers()
    {
        $qb = $this->createQueryBuilder('user')
            ->where('user.isActive = false')
            ->andWhere('user.isConfirmed = false')
            ->orderBy('user.apartmentNumber', 'ASC')
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getBlockedUsers()
    {
        $qb = $this->createQueryBuilder('user')
            ->where('user.isActive = false')
            ->andWhere('user.isConfirmed = true')
            ->orderBy('user.apartmentNumber', 'ASC')
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getActiveUsersCount()
    {
        $qb = $this->createQueryBuilder('user')
            ->select('count(user.id)')
            ->where('user.isActive = true')
            ->andWhere('user.isConfirmed = true')
            ->andWhere('user.email != :rootUser')
            ->setParameter('rootUser', self::ROOT_USER)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewUsersCount()
    {
        $qb = $this->createQueryBuilder('user')
            ->select('count(user.id)')
            ->where('user.isActive = false')
            ->andWhere('user.isConfirmed = false')
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getBlockedUsersCount()
    {
        $qb = $this->createQueryBuilder('user')
            ->select('count(user.id)')
            ->where('user.isActive = false')
            ->andWhere('user.isConfirmed = true')
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }
}
