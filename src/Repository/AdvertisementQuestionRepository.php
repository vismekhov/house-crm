<?php

namespace App\Repository;

use App\Entity\AdvertisementQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * @method AdvertisementQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdvertisementQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdvertisementQuestion[]    findAll()
 * @method AdvertisementQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertisementQuestionRepository extends ServiceEntityRepository
{
    /**
     * AdvertisementQuestionRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdvertisementQuestion::class);
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewAdsQuestionsCount()
    {
        $qb = $this->createQueryBuilder('questions')
            ->select('count(questions.id)')
            ->where('questions.isAnswered = false')
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $advertisementId
     *
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewAdsQuestionsCountById(int $advertisementId)
    {
        $qb = $this->createQueryBuilder('questions')
            ->select('count(questions.id)')
            ->where('questions.isAnswered = false')
            ->andWhere('questions.advertisement = :advertisementId')
            ->setParameter('advertisementId', $advertisementId)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param int $advertisementId
     *
     * @return mixed
     */
    public function getNewQuestionsByAd(int $advertisementId)
    {
        $qb = $this->createQueryBuilder('questions')
            ->where('questions.isAnswered = false')
            ->andWhere('questions.advertisement = :advertisementId')
            ->setParameter('advertisementId', $advertisementId)
        ;

        return $qb->getQuery()->execute();
    }
}
