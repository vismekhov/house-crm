<?php

namespace App\Repository;

use App\Entity\Feedback;
use App\Entity\FeedbackType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class FeedbackRepository
 */
class FeedbackRepository extends ServiceEntityRepository
{
    /**
     * FeedbackRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Feedback::class);
    }

    /**
     * @return mixed
     */
    public function getNewMessages()
    {
        $qb = $this->createQueryBuilder('messages')
            ->join('messages.type', 'type')
            ->where('type.code = :messagesCode')
            ->orderBy('messages.createdDate', 'DESC')
            ->setParameter('messagesCode', FeedbackType::NEW_FEEDBACK)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     */
    public function getReadMessages()
    {
        $qb = $this->createQueryBuilder('messages')
            ->join('messages.type', 'type')
            ->where('type.code = :messagesCode')
            ->orderBy('messages.createdDate', 'DESC')
            ->setParameter('messagesCode', FeedbackType::READ_FEEDBACK)
        ;

        return $qb->getQuery()->execute();
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewMessagesCount()
    {
        $qb = $this->createQueryBuilder('messages')
            ->select('count(messages.id)')
            ->join('messages.type', 'type')
            ->where('type.code = :messagesCode')
            ->setParameter('messagesCode', FeedbackType::NEW_FEEDBACK)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }
}
