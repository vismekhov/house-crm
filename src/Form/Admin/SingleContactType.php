<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SingleContactType
 */
class SingleContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('managerName', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                ],
            ])
            ->add('apartmentNumber', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'required' => false,
                ],
            ])
            ->add('phones', CollectionType::class, [
                'required' => false,
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'entry_options' => [
                    'attr' => [
                        'class' => 'form-control phone-input',
                        'required' => false,
                        'data-mask' => '(000) 000 00 00',
                        'placeholder' => '(000) 000 00 00',
                        'minlength' => 15,
                    ],
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
