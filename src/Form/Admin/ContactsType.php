<?php

namespace App\Form\Admin;

use App\Entity\Contacts;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactsType
 */
class ContactsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Сontacts title',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                ],
            ])
            ->add('isManagement', CheckboxType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('contacts', CollectionType::class, [
                'entry_type' => SingleContactType::class,
                'allow_add' => true,
                'allow_delete' => true,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contacts::class,
            'validation_groups' => ['contacts'],
            'attr' => [
                'id' => 'ContactsForm',
            ],
        ]);
    }
}
