<?php

namespace App\Form\Admin;

use App\Entity\AdvertisementAnswer;
use App\Entity\AdvertisementQuestion;
use App\Repository\AdvertisementQuestionRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertisementAnswerType
 */
class AdvertisementAnswerType extends AbstractType
{
    /**
     * @var int
     */
    private int $advertisementId;

    /**
     * @var bool
     */
    private bool $questionsType;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->advertisementId = $options['advertisementId'];
        $this->questionsType = $options['questionsType'];

        $builder
            ->add('managerQuestion', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                    'placeholder' => 'Advertisement_manager_question_placeholder',
                ],
            ])
            ->add('answer', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                    'placeholder' => 'Advertisement_answer_placeholder',
                ],
            ])
            ->add('questions', EntityType::class, [
                'class' => AdvertisementQuestion::class,
                'query_builder' => function (AdvertisementQuestionRepository $repo) {
                    return $repo->createQueryBuilder('q')
                        ->join('q.user', 'user')
                        ->where('q.advertisement = :advertisementId')
                        ->andWhere('q.isAnswered = :questionsType')
                        ->orderBy('q.createdDate', 'DESC')
                        ->setParameter('advertisementId', $this->advertisementId)
                        ->setParameter('questionsType', $this->questionsType);
                },
                'expanded' => true,
                'multiple' => true,
                'required' => false,
                'label' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdvertisementAnswer::class,
            'validation_groups' => ['advertisementAnswer'],
            'attr' => [
                'id' => 'AdvertisementAnswerForm',
            ],
        ]);

        $resolver->setRequired('advertisementId');
        $resolver->setRequired('questionsType');
        $resolver->setAllowedTypes('advertisementId', 'integer');
        $resolver->setAllowedTypes('questionsType', 'bool');
    }
}
