<?php

namespace App\Form\Admin;

use App\Entity\Advertisement;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertisementType
 */
class AdvertisementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Advertisement title',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                ],
            ])
            ->add('recap', TextareaType::class, [
                'label' => 'Recap title',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'maxLength' => 250,
                ],
            ])
            ->add('description', FroalaEditorType::class, [
                'label' => 'Description title',
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Advertisement::class,
            'validation_groups' => ['advertisement'],
            'attr' => [
                'id' => 'AdvertisementForm',
            ],
        ]);
    }
}
