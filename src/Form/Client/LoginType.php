<?php

namespace App\Form\Client;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginType
 */
class LoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Login',
                    'class' => 'form-control',
                    'email' =>  'true',
                ],
            ])
            ->add('password', PasswordType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Password',
                    'class' => 'form-control',
                    'required' => 'true',
                    'minlength' => 6,
                    'maxLength' => 50,
                ],
            ])
            ->add('rememberMe', CheckboxType::class, [
                'required' => false,
                'data' => true,
                'label' => false,
                'attr' => [
                    'class' => 'form-check-input',
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id' => 'csrf_token',
            'validation_groups' => ['authorization'],
            'attr' => [
                'id' => 'LoginForm',
            ],
        ]);
    }
}
