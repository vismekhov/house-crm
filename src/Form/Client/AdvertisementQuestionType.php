<?php

namespace App\Form\Client;

use App\Entity\AdvertisementQuestion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertisementQuestionType
 */
class AdvertisementQuestionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userQuestion', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => true,
                    'placeholder' => 'Advertisement_question_placeholder',
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdvertisementQuestion::class,
            'validation_groups' => ['advertisementQuestion'],
            'attr' => [
                'id' => 'AdvertisementQuestionForm',
            ],
        ]);
    }
}
