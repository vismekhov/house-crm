<?php

namespace App\Form\Client;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProfileType
 */
class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'disabled' => true,
                'attr' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => 'true',
                    'minlength' => 2,
                    'maxLength' => 50,
                ],
            ])
            ->add('secondName', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => 'true',
                    'minlength' => 2,
                    'maxLength' => 50,
                ],
            ])
            ->add('apartmentNumber', TextType::class, [
                'disabled' => true,
                'attr' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
            ])
            ->add('phone', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'required' => 'true',
                    'data-mask' => '(000) 000 00 00',
                    'placeholder' => '(000) 000 00 00',
                    'minlength' => 15,
                ],
            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'label' => 'Upload photo',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['profile'],
            'attr' => [
                'id' => 'ProfileForm',
            ],
        ]);
    }
}
