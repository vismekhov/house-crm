<?php

namespace App\Form\Client;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangePasswordType
 */
class ChangePasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, [
                'required' => true,
                'label' => 'Old Password',
                'attr' => [
                    'class' => 'form-control',
                    'required' => 'true',
                    'minlength' => 6,
                    'maxLength' => 50,
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'invalid_message' => 'The password fields must match',
                'first_options'  => [
                    'label' => 'New Password',
                    'attr' => [
                        'class' => 'form-control',
                        'required' => 'true',
                        'minlength' => 6,
                        'maxLength' => 50,
                    ],
                ],
                'second_options' => [
                    'label' => 'Repeat New Password',
                    'attr' => [
                        'class' => 'form-control',
                        'required' => 'true',
                        'minlength' => 6,
                        'maxLength' => 50,
                    ],
                ],

            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['profile_security'],
            'attr' => [
                'id' => 'ChangePasswordForm',
            ],
        ]);
    }
}
