<?php

namespace App\Form\Client;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationFormType
 */
class RegistrationFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email',
                    'email' =>  'true',
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'invalid_message' => 'The password fields must match',
                'first_options'  => [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                        'required' => 'true',
                        'minlength' => 6,
                        'maxLength' => 50,
                    ],
                ],
                'second_options' => [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Repeat Password',
                        'required' => 'true',
                        'minlength' => 6,
                        'maxLength' => 50,
                    ],
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'First name',
                    'required' => 'true',
                    'minlength' => 2,
                    'maxLength' => 50,
                ],
            ])
            ->add('secondName', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Second name',
                    'required' => 'true',
                    'minlength' => 2,
                    'maxLength' => 50,
                ],
            ])
            ->add('apartmentNumber', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'maxlength' => 5,
                    'class' => 'form-control',
                    'placeholder' => 'Apartment number',
                    'required' => 'true',
                ],
            ])
            ->add('phone', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Phone',
                    'required' => 'true',
                    'data-mask' => '(000) 000 00 00',
                    'minlength' => 15,
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['registration'],
            'attr' => [
                'id' => 'RegistrationForm',
            ],
        ]);
    }
}
