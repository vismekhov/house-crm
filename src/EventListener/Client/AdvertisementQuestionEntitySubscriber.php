<?php

namespace App\EventListener\Client;

use App\Entity\Advertisement;
use App\Entity\AdvertisementQuestion;
use DateTime;
use DateTimeZone;
use Exception;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AdvertisementQuestionEntitySubscriber
 */
class AdvertisementQuestionEntitySubscriber implements EventSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    /**
     * @var RequestStack
     */
    private RequestStack $request;

    /**
     * AdvertisementQuestionEntitySubscriber constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param RequestStack          $request
     */
    public function __construct(TokenStorageInterface $tokenStorage, RequestStack $request)
    {
        $this->tokenStorage = $tokenStorage;
        $this->request = $request;
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof AdvertisementQuestion) {
            return;
        }

        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!$token->isAuthenticated()) {
            return;
        }

        if (!$user = $token->getUser()) {
            return;
        }

        $advertisementQuestionId = $this->request->getCurrentRequest()->attributes->get('id');
        $advertisementQuestionEntity = $entityManager->getRepository(Advertisement::class)
            ->find($advertisementQuestionId);

        $entity->setAdvertisement($advertisementQuestionEntity);
        $entity->setUser($user);
        $entity->setCreatedDate(new DateTime('now', new DateTimeZone('Europe/Kiev')));
    }
}
