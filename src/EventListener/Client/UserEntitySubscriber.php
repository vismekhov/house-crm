<?php

namespace App\EventListener\Client;

use App\Entity\User;
use App\Entity\UserGroup;
use DateTime;
use DateTimeZone;
use Exception;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserEntitySubscriber
 */
class UserEntitySubscriber implements EventSubscriber
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * UserPasswordService constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof User) {
            return;
        }

        $isEmailExists = $entityManager->getRepository(User::class)
            ->findOneBy([
                'email' => $entity->getEmail(),
            ]);

        if ($isEmailExists) {
            throw new Exception('Email already exists');
        }

        $userGroup = $entityManager->getRepository(UserGroup::class)
            ->findOneBy([
                'name' => UserGroup::USER_NAME,
            ]);

        if ($userGroup) {
            $entity->setUserGroup($userGroup);
        }

        $entity->setRegisterDate(new DateTime('now', new DateTimeZone('Europe/Kiev')));
        $entity->setPassword(
            $this->passwordEncoder->encodePassword(
                $entity,
                $entity->getPlainPassword()
            )
        );
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {
            return;
        }

        $entity->setUpdatedAt(new DateTime('now', new DateTimeZone('Europe/Kiev')));
    }
}
