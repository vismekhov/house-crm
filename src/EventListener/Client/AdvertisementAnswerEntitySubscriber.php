<?php

namespace App\EventListener\Client;

use App\Entity\Advertisement;
use App\Entity\AdvertisementAnswer;
use DateTime;
use DateTimeZone;
use Exception;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AdvertisementAnswerEntitySubscriber
 */
class AdvertisementAnswerEntitySubscriber implements EventSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    /**
     * @var RequestStack
     */
    private RequestStack $request;

    /**
     * AdvertisementQuestionEntitySubscriber constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param RequestStack          $request
     */
    public function __construct(TokenStorageInterface $tokenStorage, RequestStack $request)
    {
        $this->tokenStorage = $tokenStorage;
        $this->request = $request;
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof AdvertisementAnswer) {
            return;
        }

        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!$token->isAuthenticated()) {
            return;
        }

        if (!$manager = $token->getUser()) {
            return;
        }

        $advertisementQuestionId = $this->request->getCurrentRequest()->attributes->get('id');
        $advertisementQuestionEntity = $entityManager->getRepository(Advertisement::class)
            ->find($advertisementQuestionId);

        if (!empty($entity->getQuestions())) {
            foreach ($entity->getQuestions() as $question) {
                $question->setIsAnswered(true);
            }
        }

        $entity->setAdvertisement($advertisementQuestionEntity);
        $entity->setManager($manager);
        $entity->setCreatedDate(new DateTime('now', new DateTimeZone('Europe/Kiev')));
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof AdvertisementAnswer) {
            return;
        }

        if (!empty($entity->getQuestions())) {
            foreach ($entity->getQuestions() as $question) {
                $entityManager->remove($question);
            }
        }
    }
}
