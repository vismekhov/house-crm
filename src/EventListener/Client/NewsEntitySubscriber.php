<?php

namespace App\EventListener\Client;

use App\Entity\News;
use DateTime;
use DateTimeZone;
use Exception;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Class NewsEntitySubscriber
 */
class NewsEntitySubscriber implements EventSubscriber
{
    /**
     * @return array|string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof News) {
            return;
        }

        $description = preg_replace('#<p data-f-id="pbf" (.*?)</p>#', '', $entity->getDescription());
        $entity->setDescription($description);
        $entity->setCreatedDate(new DateTime('now', new DateTimeZone('Europe/Kiev')));
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof News) {
            return;
        }

        $description = preg_replace('#<p data-f-id="pbf" (.*?)</p>#', '', $entity->getDescription());
        $entity->setDescription($description);
    }
}
