<?php

namespace App\EventListener\Client;

use App\Entity\Feedback;
use App\Entity\FeedbackType;
use DateTime;
use DateTimeZone;
use Exception;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class FeedbackEntitySubscriber
 */
class FeedbackEntitySubscriber implements EventSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    /**
     * ValidPasswordValidator constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof Feedback) {
            return;
        }

        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!$token->isAuthenticated()) {
            return;
        }

        if (!$user = $token->getUser()) {
            return;
        }

        $feedbackType = $entityManager->getRepository(FeedbackType::class)
            ->findOneBy([
                'code' => FeedbackType::NEW_FEEDBACK,
            ]);

        if (!$feedbackType) {
            throw new Exception('FeedbackType::NEW_FEEDBACK not found');
        }

        $entity->setUser($user);
        $entity->setCreatedDate(new DateTime('now', new DateTimeZone('Europe/Kiev')));
        $entity->setType($feedbackType);
    }
}
