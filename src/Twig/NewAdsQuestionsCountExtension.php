<?php

namespace App\Twig;

use App\Entity\AdvertisementQuestion;
use Doctrine\ORM\EntityManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class NewAdsQuestionsCountExtension
 */
class NewAdsQuestionsCountExtension extends AbstractExtension
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UsersCountExtension constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('newAdsQuestionsCount', [$this, 'getNewAdsQuestionsCount']),
            new TwigFunction('newAdsQuestionsCountByAd', [$this, 'getNewAdsQuestionsCountByAd']),
        ];
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewAdsQuestionsCount()
    {
        $newAdsQuestionsCount = $this->entityManager
            ->getRepository(AdvertisementQuestion::class)
            ->getNewAdsQuestionsCount();

        return $newAdsQuestionsCount;
    }

    /**
     * @param int $advertisementId
     *
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewAdsQuestionsCountByAd(int $advertisementId)
    {
        $newAdsQuestionsCount = $this->entityManager
            ->getRepository(AdvertisementQuestion::class)
            ->getNewAdsQuestionsCountById($advertisementId);

        return $newAdsQuestionsCount;
    }
}