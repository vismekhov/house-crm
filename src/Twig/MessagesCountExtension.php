<?php

namespace App\Twig;

use App\Entity\Feedback;
use Doctrine\ORM\EntityManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class MessagesCountExtension
 */
class MessagesCountExtension extends AbstractExtension
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UsersCountExtension constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('newMessagesCount', [$this, 'getNewMessagesCount']),
        ];
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewMessagesCount()
    {
        $newMessagesCount = $this->entityManager
            ->getRepository(Feedback::class)
            ->getNewMessagesCount();

        return $newMessagesCount;
    }
}