<?php

namespace App\Twig;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class NewUsersExtension
 */
class UsersCountExtension extends AbstractExtension
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UsersCountExtension constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('activeUsersCount', [$this, 'getActiveUsersCount']),
            new TwigFunction('newUsersCount', [$this, 'getNewUsersCount']),
            new TwigFunction('blockedUsersCount', [$this, 'getBlockedUsersCount']),
        ];
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getActiveUsersCount()
    {
        $activeUsersCount = $this->entityManager
            ->getRepository(User::class)
            ->getActiveUsersCount();

        return $activeUsersCount;
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNewUsersCount()
    {
        $newUsersCount = $this->entityManager
            ->getRepository(User::class)
            ->getNewUsersCount();

        return $newUsersCount;
    }

    /**
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getBlockedUsersCount()
    {
        $blockedUsersCount = $this->entityManager
            ->getRepository(User::class)
            ->getBlockedUsersCount();

        return $blockedUsersCount;
    }
}