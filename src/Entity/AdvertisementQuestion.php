<?php

namespace App\Entity;

use App\Entity\EntityInterface\AdvertisementInterface;
use App\Entity\EntityInterface\AdvertisementQuestionInterface;
use App\Entity\EntityInterface\UserEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdvertisementQuestionRepository")
 */
class AdvertisementQuestion implements AdvertisementQuestionInterface
{
    public const TYPE_ANSWERED = true;
    public const TYPE_NOT_ANSWERED = false;

    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var UserEntityInterface
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private UserEntityInterface $user;

    /**
     * @var AdvertisementInterface
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Advertisement")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false)
     */
    private AdvertisementInterface $advertisement;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank(message="Empty advertisementQuestion userQuestion", groups={"advertisementQuestion"})
     */
    private ?string $userQuestion = null;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     *
     * @Assert\DateTime(message="Empty advertisementQuestion dateTime", groups={"advertisementQuestion"})
     */
    private \DateTimeInterface $createdDate;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private bool $isAnswered = self::TYPE_NOT_ANSWERED;

    /**
     * @ORM\ManyToMany(targetEntity="AdvertisementAnswer", mappedBy="questions")
     */
    private $answers;

    /**
     * AdvertisementQuestion constructor.
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        return $this->userQuestion;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserEntityInterface
     */
    public function getUser(): UserEntityInterface
    {
        return $this->user;
    }

    /**
     * @param UserEntityInterface $user
     *
     * @return $this
     */
    public function setUser(UserEntityInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return AdvertisementInterface
     */
    public function getAdvertisement(): AdvertisementInterface
    {
        return $this->advertisement;
    }

    /**
     * @param AdvertisementInterface $advertisement
     *
     * @return $this
     */
    public function setAdvertisement(AdvertisementInterface $advertisement): self
    {
        $this->advertisement = $advertisement;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserQuestion(): ?string
    {
        return $this->userQuestion;
    }

    /**
     * @param string $userQuestion
     *
     * @return $this
     */
    public function setUserQuestion(string $userQuestion): self
    {
        $this->userQuestion = $userQuestion;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     *
     * @return $this
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsAnswered(): bool
    {
        return $this->isAnswered;
    }

    /**
     * @param bool $isAnswered
     *
     * @return $this
     */
    public function setIsAnswered(bool $isAnswered): self
    {
        $this->isAnswered = $isAnswered;

        return $this;
    }
}
