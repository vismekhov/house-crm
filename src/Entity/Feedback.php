<?php

namespace App\Entity;

use App\Entity\EntityInterface\FeedbackInterface;
use App\Entity\EntityInterface\FeedbackTypeInterface;
use App\Entity\EntityInterface\UserEntityInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeedbackRepository")
 */
class Feedback implements FeedbackInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var UserEntityInterface
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private UserEntityInterface $user;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $subject = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank(message="Empty message", groups={"feedback"})
     */
    private ?string $message = null;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdDate;

    /**
     * @var FeedbackTypeInterface
     *
     * @ORM\ManyToOne(targetEntity="FeedbackType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private FeedbackTypeInterface $type;

    /**
     * @return int|null
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserEntityInterface
     */
    public function getUser(): UserEntityInterface
    {
        return $this->user;
    }

    /**
     * @param UserEntityInterface $user
     *
     * @return $this
     */
    public function setUser(UserEntityInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string|null $subject
     *
     * @return $this
     */
    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDate(): DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param DateTimeInterface $createdDate
     *
     * @return $this
     */
    public function setCreatedDate(DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return FeedbackTypeInterface
     */
    public function getType(): FeedbackTypeInterface
    {
        return $this->type;
    }

    /**
     * @param FeedbackTypeInterface $type
     *
     * @return $this
     */
    public function setType(FeedbackTypeInterface $type): self
    {
        $this->type = $type;

        return $this;
    }
}
