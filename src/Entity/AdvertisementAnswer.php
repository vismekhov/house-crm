<?php

namespace App\Entity;

use App\Entity\EntityInterface\AdvertisementAnswerInterface;
use App\Entity\EntityInterface\AdvertisementInterface;
use App\Entity\EntityInterface\UserEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdvertisementAnswerRepository")
 */
class AdvertisementAnswer implements AdvertisementAnswerInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var UserEntityInterface
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id", nullable=false)
     */
    private UserEntityInterface $manager;

    /**
     * @var AdvertisementInterface
     *
     * @ORM\ManyToOne(targetEntity="Advertisement")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id", nullable=false)
     */
    private AdvertisementInterface $advertisement;

    /**
     * @ORM\ManyToMany(targetEntity="AdvertisementQuestion", inversedBy="answers", fetch="EAGER")
     * @ORM\JoinTable(name="questions_answers")
     */
    private $questions;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank(message="Empty advertisementAnswer managerQuestion", groups={"advertisementAnswer"})
     */
    private ?string $managerQuestion = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank(message="Empty advertisementAnswer answer", groups={"advertisementAnswer"})
     */
    private ?string $answer = null;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     *
     * @Assert\DateTime(message="Empty advertisementAnswer dateTime", groups={"advertisementAnswer"})
     */
    private \DateTimeInterface $createdDate;

    /**
     * AdvertisementAnswer constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserEntityInterface
     */
    public function getManager(): UserEntityInterface
    {
        return $this->manager;
    }

    /**
     * @param UserEntityInterface $manager
     *
     * @return $this
     */
    public function setManager(UserEntityInterface $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return AdvertisementInterface
     */
    public function getAdvertisement(): AdvertisementInterface
    {
        return $this->advertisement;
    }

    /**
     * @param AdvertisementInterface $advertisement
     *
     * @return $this
     */
    public function setAdvertisement(AdvertisementInterface $advertisement): self
    {
        $this->advertisement = $advertisement;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getManagerQuestion(): ?string
    {
        return $this->managerQuestion;
    }

    /**
     * @param string $managerQuestion
     *
     * @return $this
     */
    public function setManagerQuestion(string $managerQuestion): self
    {
        $this->managerQuestion = $managerQuestion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     *
     * @return $this
     */
    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     *
     * @return $this
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @param AdvertisementQuestion $question
     */
    public function addQuestion(AdvertisementQuestion $question)
    {
        $this->questions->add($question);
    }

    /**
     * @param AdvertisementQuestion $question
     */
    public function removeQuestion(AdvertisementQuestion $question)
    {
        $this->questions->removeElement($question);
    }

    public function getQuestions()
    {
        return $this->questions;
    }
}
