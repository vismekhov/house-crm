<?php

namespace App\Entity\EntityInterface;

/**
 * Interface AdvertisementAnswerInterface
 */
interface AdvertisementAnswerInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return UserEntityInterface
     */
    public function getManager(): UserEntityInterface;

    /**
     * @param UserEntityInterface $manager
     */
    public function setManager(UserEntityInterface $manager);

    /**
     * @return AdvertisementInterface
     */
    public function getAdvertisement(): AdvertisementInterface;

    /**
     * @param AdvertisementInterface $advertisement
     */
    public function setAdvertisement(AdvertisementInterface $advertisement);

    /**
     * @return string|null
     */
    public function getManagerQuestion(): ?string;

    /**
     * @param string $managerQuestion
     */
    public function setManagerQuestion(string $managerQuestion);

    /**
     * @return string|null
     */
    public function getAnswer(): ?string;

    /**
     * @param string $answer
     */
    public function setAnswer(string $answer);

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface $createdDate
     */
    public function setCreatedDate(\DateTimeInterface $createdDate);
}
