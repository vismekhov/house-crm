<?php

namespace App\Entity\EntityInterface;

use DateTimeInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Interface UserEntityInterface
 */
interface UserEntityInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string $email
     */
    public function setEmail(string $email);

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @param string $password
     */
    public function setPassword(string $password);

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string;

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword);

    /**
     * @return string|null
     */
    public function getOldPassword(): ?string;

    /**
     * @param string|null $oldPassword
     */
    public function setOldPassword(?string $oldPassword);

    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName);

    /**
     * @return string|null
     */
    public function getSecondName(): ?string;

    /**
     * @param string|null $secondName
     */
    public function setSecondName(?string $secondName);

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string;

    /**
     * @param string $apartmentNumber
     */
    public function setApartmentNumber(string $apartmentNumber);

    /**
     * @return DateTimeInterface
     */
    public function getRegisterDate(): DateTimeInterface;

    /**
     * @param DateTimeInterface $registerDate
     */
    public function setRegisterDate(DateTimeInterface $registerDate);

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $lastLogin
     */
    public function setLastLogin(?DateTimeInterface $lastLogin);

    /**
     * @return string|null
     */
    public function getPhone(): ?string;

    /**
     * @param string $phone
     */
    public function setPhone(string $phone);

    /**
     * @return bool
     */
    public function getIsActive(): bool;

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive);

    /**
     * @return bool
     */
    public function getIsConfirmed(): bool;

    /**
     * @param bool $isConfirmed
     */
    public function setIsConfirmed(bool $isConfirmed);

    /**
     * @return UserGroupInterface
     */
    public function getUserGroup(): UserGroupInterface;

    /**
     * @param UserGroupInterface $userGroup
     */
    public function setUserGroup(UserGroupInterface $userGroup);

    /**
     * @param File|null $image
     */
    public function setImageFile(File $image = null): void;

    /**
     * @return File|null
     */
    public function getImageFile(): ?File;

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void;

    /**
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?DateTimeInterface $updatedAt);
}
