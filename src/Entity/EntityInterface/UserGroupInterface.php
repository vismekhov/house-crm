<?php

namespace App\Entity\EntityInterface;

use Doctrine\Common\Collections\Collection;

/**
 * Interface UserGroupInterface
 */
interface UserGroupInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name);

    /**
     * @return array
     */
    public function getGroupCodes(): array;

    /**
     * @param array $groupCodes
     */
    public function setGroupCodes(array $groupCodes);

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description);

    /**
     * @return bool
     */
    public function getIsActive(): bool;

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive);

    /**
     * @return Collection|UserEntityInterface[]
     */
    public function getUsers(): Collection;

    /**
     * @param UserEntityInterface $user
     */
    public function addUser(UserEntityInterface $user);

    /**
     * @param UserEntityInterface $user
     */
    public function removeUser(UserEntityInterface $user);
}
