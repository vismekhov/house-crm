<?php

namespace App\Entity\EntityInterface;

/**
 * Interface ContactsInterface
 */
interface ContactsInterface
{
    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @param string $title
     */
    public function setTitle(string $title);

    /**
     * @return bool|null
     */
    public function getIsManagement(): ?bool;


    /**
     * @return array|null
     */
    public function getContacts(): ?array;

    /**
     * @param array $contacts
     */
    public function setContacts(array $contacts);

    /**
     * @param bool $isManagement
     */
    public function setIsManagement(bool $isManagement);

    /**
     * @return int|null
     */
    public function getApartmentNumber(): ?int;

    /**
     * @param int|null $apartmentNumber
     */
    public function setApartmentNumber(?int $apartmentNumber);
}
