<?php

namespace App\Entity\EntityInterface;

/**
 * Interface FeedbackTypeInterface
 */
interface FeedbackTypeInterface
{
    /**
     * @return int
     */
    public function getCode(): int;

    /**
     * @param int $code
     */
    public function setCode(int $code);

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $description
     */
    public function setDescription(string $description);
}
