<?php

namespace App\Entity\EntityInterface;

/**
 * Interface AdvertisementQuestionInterface
 */
interface AdvertisementQuestionInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return UserEntityInterface
     */
    public function getUser(): UserEntityInterface;

    /**
     * @param UserEntityInterface $user
     */
    public function setUser(UserEntityInterface $user);

    /**
     * @return AdvertisementInterface
     */
    public function getAdvertisement(): AdvertisementInterface;

    /**
     * @param AdvertisementInterface $advertisement
     */
    public function setAdvertisement(AdvertisementInterface $advertisement);

    /**
     * @return string|null
     */
    public function getUserQuestion(): ?string;

    /**
     * @param string $userQuestion
     */
    public function setUserQuestion(string $userQuestion);

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface $createdDate
     */
    public function setCreatedDate(\DateTimeInterface $createdDate);

    /**
     * @return bool
     */
    public function getIsAnswered(): bool;

    /**
     * @param bool $isAnswered
     */
    public function setIsAnswered(bool $isAnswered);
}
