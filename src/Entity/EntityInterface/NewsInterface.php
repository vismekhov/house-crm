<?php

namespace App\Entity\EntityInterface;

/**
 * Interface NewsInterface
 */
interface NewsInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @param string $title
     */
    public function setTitle(string $title);

    /**
     * @return string|null
     */
    public function getRecap(): ?string;

    /**
     * @param string|null $recap
     */
    public function setRecap(?string $recap);

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @param string $description
     */
    public function setDescription(string $description);

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface $createdDate
     */
    public function setCreatedDate(\DateTimeInterface $createdDate);
}
