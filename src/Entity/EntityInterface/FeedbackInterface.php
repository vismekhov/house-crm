<?php

namespace App\Entity\EntityInterface;

use DateTimeInterface;

/**
 * Interface FeedbackInterface
 */
interface FeedbackInterface
{
    /**
     * @return UserEntityInterface
     */
    public function getUser(): UserEntityInterface;

    /**
     * @param UserEntityInterface $user
     */
    public function setUser(UserEntityInterface $user);

    /**
     * @return string|null
     */
    public function getSubject(): ?string;

    /**
     * @param string|null $subject
     */
    public function setSubject(?string $subject);

    /**
     * @return string|null
     */
    public function getMessage(): ?string;

    /**
     * @param string $message
     */
    public function setMessage(string $message);

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDate(): DateTimeInterface;

    /**
     * @param DateTimeInterface $createdDate
     */
    public function setCreatedDate(DateTimeInterface $createdDate);

    /**
     * @return FeedbackTypeInterface
     */
    public function getType(): FeedbackTypeInterface;

    /**
     * @param FeedbackTypeInterface $type
     */
    public function setType(FeedbackTypeInterface $type);
}
