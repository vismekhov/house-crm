<?php

namespace App\Entity;

use App\Entity\EntityInterface\UserEntityInterface;
use App\Entity\EntityInterface\UserGroupInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserGroupRepository")
 */
class UserGroup implements UserGroupInterface
{
    public const ADMIN_NAME = 'admin';
    public const USER_NAME = 'user';

    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private array $groupCodes = [];

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $description;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private bool $isActive;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="userGroup")
     */
    private Collection $users;

    /**
     * UserGroup constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getGroupCodes(): array
    {
        return $this->groupCodes;
    }

    /**
     * @param array $groupCodes
     *
     * @return $this
     */
    public function setGroupCodes(array $groupCodes): self
    {
        $this->groupCodes = $groupCodes;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|UserEntityInterface[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param UserEntityInterface $user
     *
     * @return $this
     */
    public function addUser(UserEntityInterface $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setUserGroup($this);
        }

        return $this;
    }

    /**
     * @param UserEntityInterface $user
     *
     * @return $this
     */
    public function removeUser(UserEntityInterface $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getUserGroup() === $this) {
                $user->setUserGroup(null);
            }
        }

        return $this;
    }
}
