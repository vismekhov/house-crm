<?php

namespace App\Entity;

use App\Entity\EntityInterface\ContactsInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactsRepository")
 */
class Contacts implements ContactsInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Empty contacts title", groups={"contacts"})
     */
    private ?string $title = null;

    /**
     * @var array|null
     *
     * @ORM\Column(type="json")
     */
    private ?array $contacts = [];

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean")
     */
    private ?bool $isManagement = null;

    /**
     * @var int|null
     */
    private ?int $apartmentNumber = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getContacts(): ?array
    {
        return $this->contacts;
    }

    /**
     * @param array $contacts
     *
     * @return $this
     */
    public function setContacts(array $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsManagement(): ?bool
    {
        return $this->isManagement;
    }

    /**
     * @param bool $isManagement
     *
     * @return $this
     */
    public function setIsManagement(bool $isManagement): self
    {
        $this->isManagement = $isManagement;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getApartmentNumber(): ?int
    {
        return $this->apartmentNumber;
    }

    /**
     * @param int|null $apartmentNumber
     *
     * @return $this
     */
    public function setApartmentNumber(?int $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }
}
