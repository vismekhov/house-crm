<?php

namespace App\Entity;

use App\Entity\EntityInterface\UserEntityInterface;
use App\Entity\EntityInterface\UserGroupInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as CustomAssert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Security\Core\User\UserInterface;
use DateTimeInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email", groups={"registration"})
 *
 * @Vich\Uploadable
 */
class User implements UserInterface, UserEntityInterface, \Serializable
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Assert\NotBlank(message="Empty email", groups={"registration", "authorization"})
     * @Assert\Email(message="Not valid email", groups={"registration", "authorization"})
     */
    private ?string $email = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Empty password", groups={"authorization"})
     * @Assert\Length(
     *     min = 6,
     *     max = 50,
     *     minMessage = "Your password should be at least {{ limit }} characters",
     *     maxMessage = "Your password should not more than {{ limit }} characters",
     *     groups={"authorization"},
     * )
     */
    private ?string $password = null;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(message="Empty password", groups={"registration", "profile_security"})
     * @Assert\Length(
     *     min = 6,
     *     max = 50,
     *     minMessage = "Your password should be at least {{ limit }} characters",
     *     maxMessage = "Your password should not more than {{ limit }} characters",
     *     groups={"registration", "profile_security"},
     * )
     */
    private ?string $plainPassword = null;

    /**
     * @var string|null
     *
     * @Assert\NotBlank(message="Empty password", groups={"profile_security"})
     * @Assert\Length(
     *     min = 6,
     *     max = 50,
     *     minMessage = "Your password should be at least {{ limit }} characters",
     *     maxMessage = "Your password should not more than {{ limit }} characters",
     *     groups={"profile_security"},
     * )
     * @CustomAssert\ValidPassword(message="Not valid old password", groups={"profile_security"})
     */
    private ?string $oldPassword = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Empty first name", groups={"registration", "profile"})
     * @Assert\Length(
     *     min = 2,
     *     max = 50,
     *     minMessage = "Your first name should be at least {{ limit }} characters",
     *     maxMessage = "Your first name should not be more than {{ limit }} characters",
     *     groups={"registration", "profile"},
     * )
     */
    private ?string $firstName = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Empty second name", groups={"registration", "profile"})
     * @Assert\Length(
     *     min = 2,
     *     max = 50,
     *     minMessage = "Your second name should be at least {{ limit }} characters",
     *     maxMessage = "Your second name should not be more than {{ limit }} characters",
     *     groups={"registration", "profile"},
     * )
     */
    private ?string $secondName = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Empty apartment number", groups={"registration"})
     * @Assert\Length(
     *      min = 1,
     *      max = 5,
     *      maxMessage = "Wrong apartment number",
     *      allowEmptyString = false
     * )
     */
    private ?string $apartmentNumber = null;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $registerDate;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $lastLogin = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Empty phone number", groups={"registration", "profile"})
     * @Assert\PositiveOrZero(message="Wrong phone number", groups={"registration", "profile"})
     */
    private ?string $phone = null;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private bool $isActive = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private bool $isConfirmed = false;

    /**
     * @var UserGroupInterface
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\UserGroup", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private UserGroupInterface $userGroup;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $image = null;

    /**
     * @var File|null
     *
     * @Assert\Image(
     *     maxSize="5M",
     *     mimeTypes={"image/jpeg"},
     *     mimeTypesMessage="Please upload a valid photo",
     *     maxSizeMessage="Reached photo max size",
     *     groups={"profile"}
     * )
     *
     * @Vich\UploadableField(mapping="user_photos", fileNameProperty="image")
     */
    private ?File $imageFile = null;

    /**
     * @var DateTimeInterface|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $updatedAt = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->userGroup->getGroupCodes();
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string|null
     */
    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    /**
     * @param string|null $oldPassword
     */
    public function setOldPassword(?string $oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return string|void|null
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
         $this->plainPassword = null;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return $this
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    /**
     * @param string|null $secondName
     *
     * @return $this
     */
    public function setSecondName(?string $secondName): self
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string $apartmentNumber
     *
     * @return $this
     */
    public function setApartmentNumber(string $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getRegisterDate(): DateTimeInterface
    {
        return $this->registerDate;
    }

    /**
     * @param DateTimeInterface $registerDate
     *
     * @return $this
     */
    public function setRegisterDate(DateTimeInterface $registerDate): self
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTimeInterface|null $lastLogin
     *
     * @return $this
     */
    public function setLastLogin(?DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsConfirmed(): bool
    {
        return $this->isConfirmed;
    }

    /**
     * @param bool $isConfirmed
     *
     * @return $this
     */
    public function setIsConfirmed(bool $isConfirmed): self
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    /**
     * @return UserGroupInterface
     */
    public function getUserGroup(): UserGroupInterface
    {
        return $this->userGroup;
    }

    /**
     * @param UserGroupInterface $userGroup
     *
     * @return $this
     */
    public function setUserGroup(UserGroupInterface $userGroup): self
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    /**
     * @param File|null $image
     *
     * @throws \Exception
     */
    public function setImageFile(File $image = null): void
    {
        $this->imageFile = $image;

        if ($image) {
            $this->setUpdatedAt(new \DateTime('now', new \DateTimeZone('Europe/Kiev')));
        }
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @see \Serializable::serialize()
     *
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->email,
            $this->apartmentNumber,
            $this->password,
        ]);
    }

    /**
     * @see \Serializable::unserialize()
     *
     * @param string $serialized
     */
    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->email,
            $this->apartmentNumber,
            $this->password
            ) = unserialize($serialized);
    }
}
