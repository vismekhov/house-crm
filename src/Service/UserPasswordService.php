<?php

namespace App\Service;

use App\Entity\EntityInterface\UserEntityInterface;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserPasswordService
 */
class UserPasswordService
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserPasswordService constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param UserEntityInterface $user
     * @param string              $rawPassword
     *
     * @return string
     */
    public function encodePassword(UserEntityInterface $user, string $rawPassword): string
    {
        return $this->passwordEncoder->encodePassword($user, $rawPassword);
    }

    /**
     * @param UserEntityInterface $user
     *
     * @throws Exception
     */
    public function changeUserPassword(UserEntityInterface $user)
    {
        $this->validateUserPasswords($user);

        $user->setPassword(
            $this->encodePassword(
                $user,
                $user->getPlainPassword()
            )
        );
    }

    /**
     * @param UserEntityInterface $user
     * @param string              $rawPassword
     *
     * @return bool
     */
    public function isPasswordValid(UserEntityInterface $user, string $rawPassword): bool
    {
        return $this->passwordEncoder->isPasswordValid($user, $rawPassword);
    }

    /**
     * @param UserEntityInterface $user
     *
     * @throws Exception
     */
    private function validateUserPasswords(UserEntityInterface $user): void
    {
        if (empty($user->getOldPassword())) {
            throw new Exception('Empty old password.');
        }

        if (empty($user->getPlainPassword())) {
            throw new Exception('Empty password.');
        }

        if (false === $this->isPasswordValid($user, $user->getOldPassword())) {
            throw new Exception('Not valid old password.');
        }
    }
}
