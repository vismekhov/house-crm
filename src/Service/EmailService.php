<?php

namespace App\Service;

/**
 * Class EmailService
 */
class EmailService
{
    public const INITIAL_REGISTRATION = 0;
    public const APPROVE_REGISTRATION = 1;
    public const DECLINE_REGISTRATION = 2;
    public const BLOCK_USER = 3;
    public const UNBLOCK_USER = 4;
    private const SERVICE_EMAIL = 'service@tychyny16-2.kyiv.ua';

    /**
     * @var \Swift_Mailer
     */
    private \Swift_Mailer $mailer;

    /**
     * @var \Twig\Environment
     */
    private \Twig\Environment $templating;

    /**
     * EmailService constructor.
     *
     * @param \Swift_Mailer     $mailer
     * @param \Twig\Environment $templating
     */
    public function __construct(\Swift_Mailer $mailer, \Twig\Environment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param int    $action
     * @param string $email
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function send(int $action, string $email)
    {
        if (self::INITIAL_REGISTRATION === $action) {
            $message = $this->configureInitialRegistrationMail($email);
        }
        if (self::APPROVE_REGISTRATION === $action) {
            $message = $this->configureApprovalRegistrationMail($email);
        }
        if (self::DECLINE_REGISTRATION === $action) {
            $message = $this->configureDeclinedRegistrationMail($email);
        }
        if (self::BLOCK_USER === $action) {
            $message = $this->configureBlockUserMail($email);
        }
        if (self::UNBLOCK_USER === $action) {
            $message = $this->configureUnblockUserMail($email);
        }

        $this->mailer->send($message);
    }

    /**
     * @param string $email
     *
     * @return \Swift_Message
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function configureInitialRegistrationMail(string $email)
    {
        $message = (new \Swift_Message('Заявка на реєстрацію.'))
            ->setFrom(self::SERVICE_EMAIL)
            ->setTo($email)
            ->setBody(
                $this->templating->render('emails/registration/initial.html.twig'), 'text/html'
            )
        ;

        return $message;
    }

    /**
     * @param string $email
     *
     * @return \Swift_Message
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function configureApprovalRegistrationMail(string $email)
    {
        $message = (new \Swift_Message('Результати заявки на реєстрацію.'))
            ->setFrom(self::SERVICE_EMAIL)
            ->setTo($email)
            ->setBody(
                $this->templating->render('emails/registration/approve.html.twig'), 'text/html'
            )
        ;

        return $message;
    }

    /**
     * @param string $email
     *
     * @return \Swift_Message
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function configureDeclinedRegistrationMail(string $email)
    {
        $message = (new \Swift_Message('Результати заявки на реєстрацію.'))
            ->setFrom(self::SERVICE_EMAIL)
            ->setTo($email)
            ->setBody(
                $this->templating->render('emails/registration/decline.html.twig'), 'text/html'
            )
        ;

        return $message;
    }

    /**
     * @param string $email
     *
     * @return \Swift_Message
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function configureBlockUserMail(string $email)
    {
        $message = (new \Swift_Message('Блокування активності.'))
            ->setFrom(self::SERVICE_EMAIL)
            ->setTo($email)
            ->setBody(
                $this->templating->render('emails/registration/block_user.html.twig'), 'text/html'
            )
        ;

        return $message;
    }

    /**
     * @param string $email
     *
     * @return \Swift_Message
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function configureUnblockUserMail(string $email)
    {
        $message = (new \Swift_Message('Розблокування активності.'))
            ->setFrom(self::SERVICE_EMAIL)
            ->setTo($email)
            ->setBody(
                $this->templating->render('emails/registration/unblock_user.html.twig'), 'text/html'
            )
        ;

        return $message;
    }
}
