<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200218200324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questions_answers (advertisement_answer_id INT NOT NULL, advertisement_question_id INT NOT NULL, INDEX IDX_2355E6C0D24AE4D0 (advertisement_answer_id), INDEX IDX_2355E6C0F3E32DD (advertisement_question_id), PRIMARY KEY(advertisement_answer_id, advertisement_question_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE questions_answers ADD CONSTRAINT FK_2355E6C0D24AE4D0 FOREIGN KEY (advertisement_answer_id) REFERENCES advertisement_answer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE questions_answers ADD CONSTRAINT FK_2355E6C0F3E32DD FOREIGN KEY (advertisement_question_id) REFERENCES advertisement_question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user CHANGE last_login last_login DATETIME DEFAULT NULL, CHANGE image image VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE contacts CHANGE contacts contacts JSON NOT NULL');
        $this->addSql('ALTER TABLE advertisement_question DROP FOREIGN KEY FK_C8923C06AA334807');
        $this->addSql('DROP INDEX IDX_C8923C06AA334807 ON advertisement_question');
        $this->addSql('ALTER TABLE advertisement_question DROP answer_id');
        $this->addSql('ALTER TABLE feedback CHANGE user_id user_id INT DEFAULT NULL, CHANGE type_id type_id INT DEFAULT NULL, CHANGE subject subject VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group CHANGE group_codes group_codes JSON NOT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE questions_answers');
        $this->addSql('ALTER TABLE advertisement_question ADD answer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE advertisement_question ADD CONSTRAINT FK_C8923C06AA334807 FOREIGN KEY (answer_id) REFERENCES advertisement_answer (id)');
        $this->addSql('CREATE INDEX IDX_C8923C06AA334807 ON advertisement_question (answer_id)');
        $this->addSql('ALTER TABLE contacts CHANGE contacts contacts LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE feedback CHANGE user_id user_id INT DEFAULT NULL, CHANGE type_id type_id INT DEFAULT NULL, CHANGE subject subject VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE last_login last_login DATETIME DEFAULT \'NULL\', CHANGE image image VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user_group CHANGE group_codes group_codes LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE description description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
